<?php

use Illuminate\Support\Facades\Route;
use Admin\UserController;

use App\Http\Controllers\ContactUsFormController;
// use ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::resource('/profile', ProfileController::class);

// Route::resource('/courses', CourseController::class);


// ADMIN ROUTES
Route::prefix('admin')->name('admin.')->middleware(['auth', 'auth.isAdmin', 'verified'])->group(function () {
    Route::resource('/users', UserController::class);
    Route::resource('/courses', CourseController::class);
});

Route::get('/contact', [ContactUsFormController::class, 'createForm']);

Route::post('/contact', [ContactUsFormController::class, 'ContactUsForm'])->name('contact.store');

// USER ROUTES
// Route::prefix('user')->name('user.')->middleware(['auth', 'auth'])->group(function () {
//     Route::resource('/profile', UserController::class);
// });
