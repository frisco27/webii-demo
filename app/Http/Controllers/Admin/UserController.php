<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Fortify\CreateNewUser;
use App\Http\Requests\StoreUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $users = User::all();
        // return view('admin.users.index')
        //     ->with([
        //         'users' => User::all()
        //     ]);
        // return view('admin.users.index', ['users' => User::all()]);
        if(Gate::denies('logged-in')){
            dd("logeate gato");
        }
        if(Gate::allows('is-admin')) {
            return view('admin.users.index', ['users' => User::paginate(10)]);
        }
        dd('only admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create', ['roles' => Role::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        //dd($request);
        // $validatedData = $request->validated();
        // $user = User::create($validatedData);
        // $user = User::create($request->except(['_token', 'roles']));
        $newUser = new CreateNewUser();
        $user = $newUser->create($request->only(['name', 'email', 'password', 'password_confirmation']));
        $user->roles()->sync($request->roles);

        Password::sendResetLink($request->only(['email']));

        $request->session()->flash('success', 'You hava created the user');

        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.users.edit',
            [
                'user' => User::find($id),
                'roles' => Role::all()
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $user = User::findOrFail($id);
       if (!$user)
       {
           $request->session()->flash('error', 'You can no edit this user.');
           return redirect(route('admin.users.index'));
       }
       $user->update($request->except(['_token', 'roles']));
       $user->roles()->sync($request->roles);
       $request->session()->flash('success', 'You hava edited the user');

       return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //dd($id);
        User::destroy($id);
        $request->session()->flash('success', 'You hava deleted the user');
        return redirect(route('admin.users.index'));
    }
}
