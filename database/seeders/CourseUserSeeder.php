<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\User;

class CourseUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = Course::all();
        $instructors = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Instructor');
            }
        )->get();

        $instructors->each(function ($user) use ($courses){
            $user->courses()->attach(
                $courses->random(1)->pluck('id')
            );
        });
    }
}
