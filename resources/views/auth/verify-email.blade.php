@extends('templates.main', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'title' => __('Material Dashboard')])

@section('content')
<div class="content" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
      <h3>{{ __('¡Verifica tu email para comenzar a aprender en Cursos-Facil!') }} </h3>
    </div>
    {{-- <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">

    </div> --}}
  </div>
</div>
@endsection
