{{-- @extends('templates.main')

@section('content')
<form action="{{ route('register') }}" method="POST">
    @csrf
    <h2>Register</h2>
    <p class="hint-text">Create your account. It's free and only takes a minute.</p>
    <div class="form-group">
        <div class="row">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
            @enderror
            <div class="col">
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="First Name" required="required" value="{{ old('name') }}">
            </div>
        </div>
    </div>
    <div class="form-group">
        @error('email')
            <span class="invalid-feedback" role="alert">
                {{ $message }}
            </span>
        @enderror
        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" required="required" value="{{ old('email') }}">
    </div>
    <div class="form-group">
        @error('password')
            <span class="invalid-feedback" role="alert">
                {{ $message }}
            </span
        @enderror
        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required="required" value="{{ old('password') }}">
    </div>
    <div class="form-group">
        @error('password_confirmation')
            <span class="invalid-feedback" role="alert">
                {{ $message }}
            </span
        @enderror
        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Confirm Password" required="required" value="{{ old('password_confirmation') }}">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success btn-lg btn-block">Register Now</button>
    </div>
</form>
@endsection --}}
@extends('templates.main', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' => __('Material Dashboard')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Registro') }}</strong></h4>
          </div>
          <div class="card-body ">
            <p class="card-description text-center">{{ __('Inscríbete y comienza a aprender.') }}</p>
            <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fas fa-grin-alt"></i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="{{ __('Nombre completo...') }}" value="{{ old('name') }}" required autocomplete="name" autofocus>
              </div>
              @if ($errors->has('name'))
                <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fas fa-at"></i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Correo electronico...') }}" value="{{ old('email') }}" required autocomplete="email">
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fas fa-lock"></i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password...') }}" required autocomplete="password">
              </div>
              @if ($errors->has('password'))
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fas fa-key"></i>
                  </span>
                </div>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirmar Password...') }}" required autocomplete="password_confirmation">
              </div>
              @if ($errors->has('password_confirmation'))
                <div id="password_confirmation-error" class="error text-danger pl-3" for="password_confirmation" style="display: block;">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-check mr-auto ml-3 mt-3">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="policy" name="policy" {{ old('policy', 1) ? 'checked' : '' }} autocomplete="policy">
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
                {{ __('Al registrarte, aceptas nuestras ') }} <a href="#">{{ __('Condiciones de uso y Política de privacidad.') }}</a>
              </label>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Crear Cuenta') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
