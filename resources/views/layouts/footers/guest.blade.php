<footer class="footer">
    <div class="container">
        <nav class="float-left">
        <ul>
            <li>
            <a href="#">
                {{ __('Contactanos') }}
            </a>
            </li>
            <li>
            <a href="#">
                {{ __('Sobre Este Proyecto') }}
            </a>
            </li>
            <li>
            <a href="#">
                {{ __('Se un Instructor') }}
            </a>
            </li>
            <li>
            <a href="#">
                {{ __('Licencias') }}
            </a>
            </li>
        </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script> Con mucho <i class="fas fa-heart"></i> por Francisco Lusi
        </div>
    </div>
</footer>
