<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('img/laravel.svg') }}">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo">
      <a href="https://creative-tim.com/" class="simple-text logo-normal">
        {{ __('Cursos-Facil') }}
      </a>
    </div>
    <div class="sidebar-wrapper">
        @can('is-admin')
            <ul class="nav">
                <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('profile.edit', ['profile' => auth()->user()->id]) }}">
                        <i class="fas fa-user-cog"></i>
                        <span class="sidebar-normal">{{ __('User profile') }} </span>
                    </a>
                </li>

                <li class="nav-item{{ $activePage == 'user-index' ? ' active' : '' }}">
                    <a class="nav-link" href="{{route('admin.users.index')}}">
                        <i class="fas fa-users"></i>
                        <span class="sidebar-normal"> {{ __('Usuarios') }} </span>
                    </a>
                </li>

                <li class="nav-item{{ $activePage == 'course-index' ? ' active' : '' }}">
                    <a class="nav-link" href="{{route('admin.courses.index')}}">
                        <i class="fas fa-chalkboard"></i>
                        <span class="sidebar-normal"> {{ __('Cursos') }} </span>
                    </a>
                </li>
            </ul>
        @endcan
    </div>
  </div>
