<div class="wrapper ">
    @include('layouts.navbars.sidebar')
    <div class="main-panel">
        @include('layouts.navbars.navs.auth')
        @include('partials.alerts')
        @yield('content')
    </div>
    <div class="main-panel">
        @include('layouts.footers.auth')
    </div>
</div>
