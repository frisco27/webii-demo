@extends('templates.main', ['class' => 'off-canvas-sidebar', 'activePage' => 'index', 'title' => __('Material Dashboard')])

@section('content')
    {{-- <h1>Hola desde Index</h1> --}}
     @can('is-admin')
        @include('admin.home')
    @endcan
@endsection
