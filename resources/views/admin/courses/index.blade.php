@extends('templates.main', ['class' => 'off-canvas-sidebar', 'activePage' => 'course-index', 'title' => __('Administracion de Cursos')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="float-left">Administracion de <strong>cursos</strong></h2>
                    <a class="btn btn-sm btn-primary float-right padding-3" href="" role="button">Create</a>
                </div>
            </div>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#Id</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($courses as $course)
                <tr>
                <th scope="row">{{$course->id}}</th>
                    <td>{{$course->name}}</td>
                    <td>{{$course->email}}</td>
                    <td></td>

                    {{-- <td>
                        <a class="btn btn-sm btn-primary" href="{{route('admin.courses.edit', $course->id)}}" role="button">Update</a>

                        <button type="button" class="btn btn-sm btn-danger"
                            onclick="event.preventDefault();
                            document.getElementById('delete-course-form-{{$course->id}}').submit()">
                            Delete
                        </button>
                        <form id="delete-course-form-{{$course->id}}" action="{{route('admin.courses.destroy', $course->id)}}" method="POST" style="display: none;">
                            @csrf
                            @method("DELETE")

                        </form>
                    </td> --}}
                </tr>
                @endforeach
                </tbody>
            </table>
            {{$courses->links()}}
        </div>
    </div>
@endsection
