@extends('templates.main')

@section('content')
<h2>edit user</h2>
<form action="{{ route('admin.users.update', $user->id) }}" method="POST">
    @method('PATCH')
    @include('admin.users.partials.form')
</form>
@endsection
