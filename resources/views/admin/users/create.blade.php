@extends('templates.main', ['activePage' => 'users', 'title' => __('Nuevo usuario de usuario')])

@section('content')
<div class="content">
    <div class="block-header">
        <h2>Crear perfiles de Usuarios</h2>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <form action="{{ route('admin.users.store') }}" method="POST">
                @include('admin.users.partials.form', ['create' => true])
            </form>
        </div>
    </div>
</div>
@endsection
