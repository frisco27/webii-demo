@extends('templates.main', ['class' => 'off-canvas-sidebar', 'activePage' => 'user-index', 'title' => __('Administracion de Usuarios')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="float-left">Administracion de <strong>usuarios</strong></h2>
                    <a class="btn btn-sm btn-primary float-right padding-3" href="{{route('admin.users.create')}}" role="button">Create</a>
                </div>
            </div>
            {{-- <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#Id</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo Electronico</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        <a class="btn btn-sm btn-primary" href="{{route('admin.users.edit', $user->id)}}" role="button">Update</a>

                        <button type="button" class="btn btn-sm btn-danger"
                            onclick="event.preventDefault();
                            document.getElementById('delete-user-form-{{$user->id}}').submit()">
                            Delete
                        </button>
                        <form id="delete-user-form-{{$user->id}}" action="{{route('admin.users.destroy', $user->id)}}" method="POST" style="display: none;">
                            @csrf
                            @method("DELETE")

                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{$users->links()}} --}}
            <div class="card card-nav-tabs">
                <div class="card-header card-header-primary">
                  <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active show" href="#admin" data-toggle="tab">
                            <i class="fas fa-user-shield fa-2x"></i> Administrador
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#teacher" data-toggle="tab">
                            <i class="fas fa-chalkboard-teacher fa-2x"></i> Profesor
                          <div class="ripple-container"></div></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#student" data-toggle="tab">
                            <i class="fas fa-user-graduate fa-2x"></i> Alumno
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body ">
                    <div class="tab-content text-center">
                        <div class="tab-pane active show" id="admin">
                            @include('admin.users.partials.table', ['adminTable' => true])
                        </div>
                        <div class="tab-pane" id="teacher">
                            @include('admin.users.partials.table', ['teacherTable' => true])
                        </div>
                        <div class="tab-pane" id="student">
                            @include('admin.users.partials.table', ['studentTable' => true])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
