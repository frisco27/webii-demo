
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#Id</th>
        <th scope="col">Nombre</th>
        <th scope="col">Correo Electronico</th>
        <th scope="col">Acciones</th>
    </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
            @isset($adminTable)
                @if(in_array(1, $user->roles->pluck('id')->toArray()))
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('admin.users.edit', $user->id)}}" role="button">Update</a>
                            <button type="button" class="btn btn-sm btn-danger"
                                onclick="event.preventDefault();
                                document.getElementById('delete-user-form-{{$user->id}}').submit()">
                                Delete
                            </button>
                            <form id="delete-user-form-{{$user->id}}" action="{{route('admin.users.destroy', $user->id)}}" method="POST" style="display: none;">
                                @csrf
                                @method("DELETE")
                            </form>
                        </td>
                    </tr>
                @endif
            @endisset
            @isset($teacherTable)
                @if(in_array(2, $user->roles->pluck('id')->toArray()))
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('admin.users.edit', $user->id)}}" role="button">Update</a>
                            <button type="button" class="btn btn-sm btn-danger"
                                onclick="event.preventDefault();
                                document.getElementById('delete-user-form-{{$user->id}}').submit()">
                                Delete
                            </button>
                            <form id="delete-user-form-{{$user->id}}" action="{{route('admin.users.destroy', $user->id)}}" method="POST" style="display: none;">
                                @csrf
                                @method("DELETE")
                            </form>
                        </td>
                    </tr>
                @endif
            @endisset
            @isset($studentTable)
                @if(in_array(3, $user->roles->pluck('id')->toArray()))
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('admin.users.edit', $user->id)}}" role="button">Update</a>
                            <button type="button" class="btn btn-sm btn-danger"
                                onclick="event.preventDefault();
                                document.getElementById('delete-user-form-{{$user->id}}').submit()">
                                Delete
                            </button>
                            <form id="delete-user-form-{{$user->id}}" action="{{route('admin.users.destroy', $user->id)}}" method="POST" style="display: none;">
                                @csrf
                                @method("DELETE")
                            </form>
                        </td>
                    </tr>
                @endif
            @endisset
        @endforeach
    </tbody>
</table>
{{$users->links()}}
