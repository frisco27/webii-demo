@csrf
<div class="mb-3">
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="First Name"  value="{{ old('name') }}
        @isset($user) {{$user->name}} @endisset">
        @error('name')
            <span class="invalid-feedback" role="alert">
                {{ $message }}
            </span>
        @enderror
</div>
<div class="mb-3">
    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email"  value="{{ old('email') }}
    @isset($user) {{$user->email}} @endisset">
    @error('email')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>
@isset($create)
    <div class="mb-3">
        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password"  value="{{ old('password') }}">
        @error('password')
            <span class="invalid-feedback" role="alert">
                {{ $message }}
            </span>
        @enderror
    </div>
@endisset
<div class="mb-3">
    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Password"  value="{{ old('password_confirmation') }}" id="password">
    @error('password_confirmation')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>
<div class="mb-3">
    @foreach($roles as $role)
        <div class="form-check">
            <input class="form-check-input" name="roles[]"
            type="checkbox" value="{{$role->id}}" id="{{$role->name}}"
            @isset($user) @if(in_array($role->id, $user->roles->pluck('id')->toArray())) checked @endif @endisset>
            <label class="form-check-label" for="{{$role->name}}">
                {{$role->name}}
            </label>
        </div>
    @endforeach
</div>
<div class="mb-3">
    <button type="submit" class="btn btn-success btn-lg btn-block">enviar</button>
</div>
